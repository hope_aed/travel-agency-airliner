/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Bussiness;

/**
 *
 * @author don
 */
public class Customer {
    private String customerName;
    private String customerContactNumber;
    private String customerFlightName;
    private String customerFlightnumber;
    private String customerSource;
    private String customerDestiny;
    private String customerdate; 
    
    

    /**
     *
     */
   

    /**
     *
     */ 
      public Customer() {
    }

    public Customer(String customerName, String customerContactNumber,String customerFlightName,String customerFlightnumber,String customerSource, String customerDestiny,String customerdate) {
       this.customerName = customerName;
       this.customerContactNumber = customerContactNumber;
       this.customerDestiny = customerDestiny;
       this.customerFlightName = customerFlightName;
       this.customerFlightnumber = customerFlightnumber;
       this.customerSource = customerSource;
       this.customerdate = customerdate;
       
    }

    public String getCustomerName() {
        return customerName;
    }

    public void setCustomerName(String customerName) {
        this.customerName = customerName;
    }

    public String getCustomerContactNumber() {
        return customerContactNumber;
    }

    public void setCustomerContactNumber(String customerContactNumber) {
        this.customerContactNumber = customerContactNumber;
    }

    public String getCustomerFlightName() {
        return customerFlightName;
    }

    public void setCustomerFlightName(String customerFlightName) {
        this.customerFlightName = customerFlightName;
    }

    public String getCustomerFlightnumber() {
        return customerFlightnumber;
    }

    public void setCustomerFlightnumber(String customerFlightnumber) {
        this.customerFlightnumber = customerFlightnumber;
    }

    public String getCustomerSource() {
        return customerSource;
    }

    public void setCustomerSource(String customerSource) {
        this.customerSource = customerSource;
    }

    public String getCustomerDestiny() {
        return customerDestiny;
    }

    public void setCustomerDestiny(String customerDestiny) {
        this.customerDestiny = customerDestiny;
    }

    public String getCustomerdate() {
        return customerdate;
    }

    public void setCustomerdate(String customerdate) {
        this.customerdate = customerdate;
    }
    
    
    
    
       @Override
    public String toString(){
        return customerName;
    }
    
}
